// lab1_var5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cmath>
#include <iostream>

static const double pi{ 3.1415 };

class square {
    int side;

public:
    square(int s) : side(s) {}
    double get_square() { 
        return static_cast<double>(side * side); 
    }
};

class circle {
    int R;

public:
    circle(int r) : R(r) {}
    double get_square() {  
        return pi * R * R; 
    }
};

class triangle {
    int side;

public:
    triangle(int s) : side(s) {}
    double get_square() { 
        return (std::sqrt(3) * std::pow(side, 2)) / 4.0; 
    }
};

template<typename T, int length>
class figure {
    T object;

public:
    figure() : object(T(length)) {}

    double get_square() {
        return object.get_square();
    }
};

int main()
{
    figure<square, 2> sq;
    figure<circle, 2> cr;
    figure<triangle, 2> tr;

    std::cout << "Square's square: "   << sq.get_square() << std::endl
              << "Circle's square: "   << cr.get_square() << std::endl
              << "Triangle's square: " << tr.get_square() << std::endl;

    return 0;
}

