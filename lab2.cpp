// lab2_var5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main()
{
    std::vector<int> vec{ 1, 6, 8, 4, 5, 98, 0, 13 };
    std::vector<int> sorted;

    for (auto a : vec) {
        std::cout << a << " ";
    }
    std::cout << std::endl;

    std::copy(vec.begin(), vec.end(), std::back_inserter(sorted));

    std::sort(sorted.begin(), sorted.end());

    vec.insert(vec.begin(), sorted.front());

    auto iter = vec.begin();
    auto size = vec.size() / 2 + 1;
    std::advance(iter, size);

    vec.insert(iter, sorted.back());

    for (auto a : vec) {
        std::cout << a << " ";
    }
    std::cout << std::endl;

    return 0;
}
