// lab3_var5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>

bool is_perfect_square(unsigned n)
{
    if (n < 0) {
        return false;
    }

    if (n < 2) {
        return true;
    }

    int i = 1;
    int sum = 0;
    while (sum < n) {
        sum += i;
        i += 2;
    }

    return sum == n;
}

bool is_fibonacci(unsigned n)
{
    return is_perfect_square(5 * n*n + 4) \
           || is_perfect_square(5 * n*n - 4);
}


int main()
{
    std::vector<unsigned> fib{ 56, 0, 1, 3, 67, 5, 8, 21, 99, 34, 7445, 6765 };

    unsigned sum;
    std::for_each(fib.begin(), fib.end(), [&](unsigned n)
    {
        if (is_fibonacci(n)) {
            std::cout << n << " ";
            sum += n;
        }
    });

    std::cout << "Sum is: " << sum << std::endl;

    return 0;
}

